# About

The ChildBrowser allows you to display external webpages within your [Cordova](http://incubator.apache.org/cordova/) application.

* For back support reference:<br> 
[https://github.com/phonegap/phonegap-plugins/tree/master/iPhone/ChildBrowser 
](https://github.com/phonegap/phonegap-plugins/tree/master/iPhone/ChildBrowser)


A simple use case would be:

* Users can follow links/buttons to view web content without leaving your app. 
* Display web pages/images/videos/pdfs in the ChildBrowser.

This command creates a popup browser that is shown in front of your app, when the user presses the DONE button they are simply returned to your app ( actually they never left ).

The ChildBrowser has buttons for refreshing, navigating back + forwards, as well as the option to open in Safari.

Icons are located in the [ChildBrowser.bundle](https://github.com/phonegap/phonegap-plugins/tree/master/iOS/ChildBrowser/ChildBrowser.bundle) and can be customized. Added Retina -72@2x.png image support to fix small icons in the webview toolbar when displayed on Retina devices.

- Added Temporary Scope (self executing) per [Cordova Plugin Upgrade Guide](https://github.com/phonegap/phonegap-plugins/blob/master/iOS/README.md).


# To install

* Add the `ChildBrowserCommand.h`, `ChildBrowserCommand.m`, `ChildBrowserViewController.h`, `ChildBrowserViewController.m` and `ChildBrowserViewController.xib` files to your Plugins Folder (preferably through drag'n'drop to Xcode).

* Place the `ChildBrowser.js` file in your app root, and include it from your html.

* Add Key `ChildBrowserCommand` and Value `ChildBrowserCommand` to the `Cordova.plist` in your application Xcode project.
  ![image](https://github.com/phonegap/phonegap-plugins/raw/master/iOS/ChildBrowser/Cordova.plist.png)

*Note: If you prefer a simple browser view, replace the `ChildBrowserViewController.xib` file with `ChildBrowserViewController-simple.xib` - useful for displaying PDFs, images, etc.*


# Using the plugin

Here is a sample command to open Google in a ChildBrowser :

```javascript
		window.plugins.childBrowser.show( "http://www.google.com" );
```