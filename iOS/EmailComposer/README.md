* You will need to add `MessageUI.framework` to your project if it is not already included (Build Phases -> Link Binary With Libraries).

* Add the `EmailComposer.h` and `EmailComposer.m`  files to your Plugins Folder (preferably through drag'n'drop to Xcode).

* Place the `EmailComposer.js` file in your app root, and include it from your html.

* Add to Cordova.plist Plugins: key `EmailComposer` value `EmailComposer`.

* To call the EmailComposer, use the following code:
```javascript
		window.plugins.emailComposer.showEmailComposer(subject,body,toRecipients,ccRecipients,bccRecipients,bIsHTML);
```