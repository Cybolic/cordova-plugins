//
//  EmailComposer.js
//  Accessable at window.plugins.emailComposer
// 
//
//  Created by Jesse MacFadyen on 10-04-05.
//  Copyright 2010 Nitobi. All rights reserved.
//
//	Updated to work with Cordova 2.1 by Christian Dannie Storgaard on 2012-10-12.
//

(function(){

	var cordovaRef = window.PhoneGap || window.Cordova || window.cordova; // old to new fallbacks

	function EmailComposer() {
		this.resultCallback = null; // Function
	}

	EmailComposer.ComposeResultType = {
		Cancelled:0,
		Saved:1,
		Sent:2,
		Failed:3,
		NotSent:4
	}


	// showEmailComposer : all args optional

	EmailComposer.prototype.showEmailComposer = function(subject,body,toRecipients,ccRecipients,bccRecipients,bIsHTML) {
		var args = {};
		if(toRecipients)
			args.toRecipients = toRecipients;
		if(ccRecipients)
			args.ccRecipients = ccRecipients;
		if(bccRecipients)
			args.bccRecipients = bccRecipients;
		if(subject)
			args.subject = subject;
		if(body)
			args.body = body;
		if(bIsHTML)
			args.bIsHTML = bIsHTML;

		cordovaRef.exec("EmailComposer.showEmailComposer", args);
	}

	// this will be forever known as the orch-func -jm
	EmailComposer.prototype.showEmailComposerWithCB = function(cbFunction,subject,body,toRecipients,ccRecipients,bccRecipients,bIsHTML) {
	  this.resultCallback = cbFunction;
	  this.showEmailComposer.apply(this,[subject,body,toRecipients,ccRecipients,bccRecipients,bIsHTML]);
	}

	EmailComposer.prototype._didFinishWithResult = function(res) {
		this.resultCallback(res);
	}



	EmailComposer.install = function() {
	    if (!window.plugins) {
	        window.plugins = {};
	    }
	        if (!window.plugins.emailComposer) {
	        window.plugins.emailComposer = new EmailComposer();
	    }
	};


	if (cordovaRef && cordovaRef.addConstructor) {
	    cordovaRef.addConstructor(EmailComposer.install);
	} else {
	    console.log("EmailComposer Cordova Plugin could not be installed.");
	    return null;
	}

})()