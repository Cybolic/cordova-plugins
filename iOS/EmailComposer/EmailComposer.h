//
//  EmailComposer.h
//
//
//  Created by Jesse MacFadyen on 10-04-05.
//  Copyright 2010 Nitobi. All rights reserved.
//
//	Updated to work with Cordova 2.1 by Christian Dannie Storgaard on 2012-10-12.
//

#import <Foundation/Foundation.h>
#import <MessageUI/MFMailComposeViewController.h>

#import <Cordova/CDVPlugin.h>

@interface EmailComposer : CDVPlugin < MFMailComposeViewControllerDelegate > {
}

- (void) showEmailComposer:(NSMutableArray*)arguments withDict:(NSMutableDictionary*)options;

@end
